package com.kleematik.pandas.inventory.infra.data.repository;

import com.kleematik.pandas.inventory.application.domain.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, String> {
    Optional<Inventory> findBySkuCode(String skuCode);
}
