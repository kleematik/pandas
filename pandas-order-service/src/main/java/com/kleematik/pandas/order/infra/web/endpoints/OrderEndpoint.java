package com.kleematik.pandas.order.infra.web.endpoints;

import com.kleematik.pandas.order.application.dto.OrderRequest;
import com.kleematik.pandas.order.application.service.PlaceOrderService;
import com.kleematik.pandas.order.infra.web.feign.GetProductStockBySkuCodeFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderEndpoint {

    private final PlaceOrderService placeOrderService;
    private final GetProductStockBySkuCodeFeignClient getProductStockBySkuCodeClient;

    @PostMapping("/")
    public String execute(@RequestBody OrderRequest request) {

        final boolean isStockAvailable = request.getOrderItems().stream()
                .allMatch(orderItemVO ->
                        getProductStockBySkuCodeClient.execute(orderItemVO.asDomain().getSkuCode()));

        if (isStockAvailable) {
            placeOrderService.execute(request);
            return "Order was placed successfully.";
        }
        return "Order failed. Please try again";
    }
}


