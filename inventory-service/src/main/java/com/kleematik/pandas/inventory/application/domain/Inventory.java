package com.kleematik.pandas.inventory.application.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "inventory", schema = "pandascatalog")
public class Inventory {
    @Id
    @GeneratedValue(generator = "uuid_generator")
    @GenericGenerator(name = "uuid_generator", strategy = "uuid")
    @Column(
            name = "id",
            nullable = false,
            columnDefinition = "VARCHAR(32)"
    )
    private String id;
    @Column(
            name = "sku_code",
            nullable = false,
            columnDefinition = "VARCHAR(32)"
    )
    private String skuCode;
    @Column(
            nullable = false,
            columnDefinition = "numeric(12) DEFAULT 0"
    )
    private Long stock;

}
