package com.kleematik.pandas.order.application.service;

import com.kleematik.pandas.order.application.dto.OrderRequest;

public interface PlaceOrderService {

    void execute(OrderRequest request);
}
