package com.kleematik.pandas.product.infrastructure.repository.impl;

import com.kleematik.pandas.product.application.dto.ProductResponse;
import com.kleematik.pandas.product.application.mapper.MapperProductResponse;
import com.kleematik.pandas.product.application.service.GetProducts;
import com.kleematik.pandas.product.infrastructure.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class GetProductsImpl implements GetProducts {

    private final ProductRepository productRepository;
    private final MapperProductResponse mapper;

    @Override
    public List<ProductResponse> execute() {
        return productRepository.findAll().stream()
                .map(mapper::transform)
                .collect(Collectors.toList());

    }
}
