package com.kleematik.pandas.product.infrastructure.repository.impl;

import com.kleematik.pandas.product.application.dto.ProductRequest;
import com.kleematik.pandas.product.application.mapper.MapperProductRequest;
import com.kleematik.pandas.product.infrastructure.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest(
        classes = { CreateProductImpl.class}
)
class CreateProductTest {

    @Mock
    private ProductRepository mockProductRepository;

    @Mock
    private MapperProductRequest mapper;

    @InjectMocks
    private CreateProductImpl underTest;

    @Test
    void itShouldCreateProduct() {
        final ProductRequest request = ProductRequest.builder()
                .name("New Product")
                .description("New Description")
                .price(new BigDecimal("20.99"))
                .build();

        underTest.execute(request);
        Mockito.verify(mockProductRepository, Mockito.times(1))
                .save(Mockito.any());
    }
}