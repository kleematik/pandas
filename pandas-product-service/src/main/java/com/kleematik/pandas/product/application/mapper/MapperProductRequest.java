package com.kleematik.pandas.product.application.mapper;

import com.kleematik.pandas.product.application.dto.ProductRequest;
import com.kleematik.pandas.product.domain.Product;
import org.springframework.stereotype.Service;

@Service
public class MapperProductRequest implements Mapper<ProductRequest, Product> {
    @Override
    public Product transform(ProductRequest request) {
        return Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .price(request.getPrice())
                .build();
    }
}
