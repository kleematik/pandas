package com.kleematik.pandas.inventory.infra.web.endpoints;

import com.kleematik.pandas.inventory.application.service.GetProductStockBySkuCode;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/inventory")
@RequiredArgsConstructor
public class InventoryEndpoint {

    private final GetProductStockBySkuCode getProductStockBySkuCode;

    @GetMapping("/{sku_code}")
    @ResponseBody
    public boolean isProductAvailable(@PathVariable("sku_code") String request) {
        return getProductStockBySkuCode.execute(request).getStock() > 0;
    }
}
