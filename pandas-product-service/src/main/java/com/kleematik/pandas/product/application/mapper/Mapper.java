package com.kleematik.pandas.product.application.mapper;

public interface Mapper<FROM, TO> {
    TO transform(FROM from);
}
