package com.kleematik.pandas.order.application.vo;

import com.kleematik.pandas.order.application.domain.Order;
import com.kleematik.pandas.order.application.domain.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public final class OrderItemVO {
    private String skuCode;
    private BigDecimal price;
    private Integer quantity;

    public OrderItem asDomain() {
        return OrderItem.builder()
                .price(this.price)
                .skuCode(this.skuCode)
                .quantity(this.quantity)
                .build();
    }
}