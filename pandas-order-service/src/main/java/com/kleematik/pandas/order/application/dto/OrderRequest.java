package com.kleematik.pandas.order.application.dto;

import com.kleematik.pandas.order.application.vo.OrderItemVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderRequest {
    private List<OrderItemVO> orderItems;
}
