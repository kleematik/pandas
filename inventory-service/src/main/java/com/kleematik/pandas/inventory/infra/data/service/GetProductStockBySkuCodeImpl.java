package com.kleematik.pandas.inventory.infra.data.service;

import com.kleematik.pandas.inventory.application.domain.Inventory;
import com.kleematik.pandas.inventory.application.service.GetProductStockBySkuCode;
import com.kleematik.pandas.inventory.infra.data.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetProductStockBySkuCodeImpl implements GetProductStockBySkuCode {

    private final InventoryRepository inventoryRepository;

    @Override
    public Inventory execute(String skuCode) {
        return inventoryRepository.findBySkuCode(skuCode)
                .orElseThrow(() -> new RuntimeException("cannot find product by sku code " + skuCode));
    }
}
