package com.kleematik.pandas.inventory.application.service;

import com.kleematik.pandas.inventory.application.domain.Inventory;

public interface GetProductStockBySkuCode {

    Inventory execute(String skuCode);
}
