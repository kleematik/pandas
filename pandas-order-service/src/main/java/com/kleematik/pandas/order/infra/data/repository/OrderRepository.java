package com.kleematik.pandas.order.infra.data.repository;

import com.kleematik.pandas.order.application.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, String> {
}
