package com.kleematik.pandas.order.infra.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "pandas-inventory-service")
public interface GetProductStockBySkuCodeFeignClient {
    @GetMapping("/api/inventory/{sku_code}")
    boolean execute(@PathVariable("sku_code") String request);
}
