package com.kleematik.pandas.order.infra.data.service;

import com.kleematik.pandas.order.application.domain.Order;
import com.kleematik.pandas.order.application.domain.OrderItem;
import com.kleematik.pandas.order.application.dto.OrderRequest;
import com.kleematik.pandas.order.application.service.PlaceOrderService;
import com.kleematik.pandas.order.application.vo.OrderItemVO;
import com.kleematik.pandas.order.infra.data.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlaceOrderServiceImpl implements PlaceOrderService {

    private final OrderRepository orderRepository;

    @Override
    public void execute(OrderRequest request) {
        final List<OrderItem> orderItems = request.getOrderItems().stream()
                .map(OrderItemVO::asDomain)
                .collect(Collectors.toList());

        final Order order = Order.builder()
                .orderNumber(UUID.randomUUID().toString())
                .orderItems(orderItems)
                .build();
        orderRepository.save(order);
    }
}
