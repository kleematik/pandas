package com.kleematik.pandas.product.application.service;

import com.kleematik.pandas.product.application.dto.ProductRequest;

public interface CreateProduct {
    void execute(ProductRequest request);
}
