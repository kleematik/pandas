package com.kleematik.pandas.product.infrastructure.repository;

import com.kleematik.pandas.product.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
