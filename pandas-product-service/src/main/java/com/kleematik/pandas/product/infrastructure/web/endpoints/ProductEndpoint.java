package com.kleematik.pandas.product.infrastructure.web.endpoints;

import com.kleematik.pandas.product.application.dto.ProductRequest;
import com.kleematik.pandas.product.application.dto.ProductResponse;
import com.kleematik.pandas.product.application.service.CreateProduct;
import com.kleematik.pandas.product.application.service.GetProducts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
@RefreshScope
public class ProductEndpoint {

    private final GetProducts getProducts;
    private final CreateProduct createProduct;

    @Value("${pandas.product.test-name}")
    private String testName;

    @GetMapping
    @ResponseBody
    public List<ProductResponse> list() {
        return getProducts.execute();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void create(@RequestBody ProductRequest request) {
        createProduct.execute(request);
    }

    @GetMapping("/test")
    @ResponseBody
    public String test() {
        return testName;
    }
}
