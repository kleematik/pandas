package com.kleematik.pandas.order.application.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity(name = "order")
@Table(name = "order", schema = "pandasorder")
public class Order {
    @Id
    @GeneratedValue(generator = "uuid_generator")
    @GenericGenerator(
            name = "uuid_generator",
            strategy = "uuid"
    )
    @Column(name = "id", nullable = false, columnDefinition = "VARCHAR(32)")
    private String id;
    @Column(name = "order_number", nullable = false)
    private String orderNumber;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;
}
