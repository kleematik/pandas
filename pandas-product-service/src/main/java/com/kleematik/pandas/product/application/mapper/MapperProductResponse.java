package com.kleematik.pandas.product.application.mapper;

import com.kleematik.pandas.product.application.dto.ProductResponse;
import com.kleematik.pandas.product.domain.Product;
import org.springframework.stereotype.Service;

@Service
public class MapperProductResponse implements Mapper<Product, ProductResponse> {
    @Override
    public ProductResponse transform(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }
}
