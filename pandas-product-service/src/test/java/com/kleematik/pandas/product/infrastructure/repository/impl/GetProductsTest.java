package com.kleematik.pandas.product.infrastructure.repository.impl;

import com.kleematik.pandas.product.application.dto.ProductResponse;
import com.kleematik.pandas.product.application.mapper.MapperProductResponse;
import com.kleematik.pandas.product.domain.Product;
import com.kleematik.pandas.product.infrastructure.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class GetProductsTest {

    @Mock
    private ProductRepository mockProductRepository;

    @Mock
    private MapperProductResponse mapper;

    @InjectMocks
    private GetProductsImpl underTest;

    @Test
    void itShouldReturnProductList() {

        // given
        final List<Product> list = new ArrayList<>();
        list.add(Product.builder()
                .id(UUID.randomUUID().toString())
                .name("Test Product")
                .description("Product description")
                .price(new BigDecimal("20.99"))
                .build()
        );
        Mockito.when(mockProductRepository.findAll()).thenReturn(list);

        // when
        final List<ProductResponse> resp = underTest.execute();

        // then
        assertThat(resp.size()).isGreaterThanOrEqualTo(0);
        Mockito.verify(mockProductRepository, Mockito.times(1)).findAll();
    }
}