package com.kleematik.pandas.product.application.service;

import com.kleematik.pandas.product.application.dto.ProductResponse;

import java.util.List;

public interface GetProducts {
    List<ProductResponse> execute();
}
