package com.kleematik.pandas.product.application.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class ProductRequest {
    private BigDecimal price;
    private String name;
    private String description;
}
