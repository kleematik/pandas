package com.kleematik.pandas.product.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity(name = "product")
@Table(schema = "pandasproduct", name = "product")
public class Product {
    @Id
    @GeneratedValue(generator = "uuid_generator")
    @GenericGenerator(
            name = "uuid_generator",
            strategy = "uuid"
    )
    @Column(name = "id",
            nullable = false,
            updatable = false,
            columnDefinition = "VARCHAR(32)"
    )
    private String id;
    @Column(nullable = false)
    private String name;
    @Column(columnDefinition = "text")
    private String description;
    @Column(nullable = false)
    private BigDecimal price;
}
