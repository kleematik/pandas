package com.kleematik.pandas.product.infrastructure.repository.impl;

import com.kleematik.pandas.product.application.dto.ProductRequest;
import com.kleematik.pandas.product.application.mapper.MapperProductRequest;
import com.kleematik.pandas.product.application.service.CreateProduct;
import com.kleematik.pandas.product.infrastructure.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class CreateProductImpl implements CreateProduct {

    private final ProductRepository productRepository;
    private final MapperProductRequest mapper;

    @Override
    @Transactional
    public void execute(ProductRequest request) {
        productRepository.save(mapper.transform(request));
    }
}
